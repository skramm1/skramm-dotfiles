/**
 * ROFI Skramm theme.
 * User: Kea
 * Based on Documentation by Qball, Copyright Dave Davenport
 */

/** Default settings, every widget inherits from this. */
* {
    /** Default background color is transparent. */
    background-color: transparent;
    /** Default text is white */
    text-color:       white;
}

/**
 * Entry box on top. 
 */
entry {
    /** top  and bottom border */
    border: 1px 0px;
    /** Dark grey border color */
    border-color:  darkgrey;
    /** Background is grey */
    background-color: #26292e;
    /** 4px padding on the inside of the border. */
    padding:       4px;
    /** when no text is set, show 'Type to filter' */
    placeholder:       "Filtrer...";
    /** this text is dark grey */
    placeholder-color: darkgrey;
    /** inherit font setting from parent */
    font: inherit;
    cursor: text;
}

/**
 * Input bar
 */
inputbar {
    /** no spacing between widgets */
    spacing: 0;
    /** include entry and mode-switcher (removes prompt) */
    children: [  icon-keyboard, entry, mode-switcher ];
    /** use monospace font. */
    font:   "ubuntu 18";
}

/**
 * Mode switcher.
 *  We set it up to 'connect' to reset of input bar.
 */
mode-switcher {
    expand: true;
    /** we use spacing between children to draw a 'border' */
    spacing: 2px;
    border: 1px;
    border-radius: 0px 4px 4px 0px;
    /** border and background are same color, widget will have desired bg color.*/
    /** this way the spacing shows as a border */
    border-color: darkgrey;
    background-color: darkgrey;
    /** inherit font setting from parent */
    font: inherit;
}

/**
 * Buttons in mode switcher.
 */
button {
    background-color: #26292e;
    border-color: darkgrey;
    /** inherit font setting from parent */
    font: inherit;
    cursor: pointer;
}

/**
 * Selected buttons in mode switcher.
 */
button selected {
    background-color: #69707c;
    text-color:       #28e2d2;
}

/**
 * Small icon in inputbar
 */
icon-keyboard {
    /** give it a 2 pixel border, except on the right side. */
    border:        1px 0px 1px 1px;
    /** with a radius on the left two corners. */ 
    border-radius: 4px 0px 0px 4px;
    /** add matching border. */
    border-color: darkgrey;
    /** match background. */
    background-color:  #26292e;
    /** move icon away from right border. */
    padding: 0px 10px 0px 10px;
    /** Only use required space. */
    expand: false;
    /** icon is around 1.2 font width */
    size: 1.2em;
    /** Icon name, we use symbolic name here */
    filename: "keyboard";
}

/**
 * Main window widget
 */
window {
    /** Place on top center of rofi window on the top center of the screen. */
    anchor: north;
    location: north;

    /** 100% screen width */
    width:            100%;
    height:100%;
    margin:10px;

    /** Black transparent color. */
    background-color: black / 40%;
//    background-color: transparent;
    /** Small one 1 font width border on inside of window. */
    padding:           1em;

    /** border */
    border-color: black;
    border:  1px;
}

/**
 * Main container in the window.
 */
mainbox { 
    /** spacing between widgets */
    spacing: 1em;
}

/**
 * listview that shows entries.
 */
listview {
//layout:horizontal;
dynamic:true;
    /** 4 rows. */
//   lines: 4;
    /** 6 columns */
    columns: 6;
    /** add 1 em spacing between items */
    spacing: 1em;
    /** Don't reduce columns if less items are available. */
    fixed-columns: true;
    fixed-lines: false;
}
/**
 * entry in listview.
 */
element {
    /** clients are packed vertically. */
    orientation:      vertical;
    /** 2 px border */
    border:           1px;
    /** with 4px radius on corners. */
    border-radius:    5px ;
    border-color:     black;
    background-color: #26292e;
    /** 4 px padding on the inside of border */
    cursor: pointer;
    padding:          4px;
}

/** Entry text */
element-text {
    /* align font in (horizontally) center */
    horizontal-align: 0.5;
    cursor: inherit;
    color:inherit;
}


/** selected element */
element selected {
    text-color: #28e2d2;
    text-style:bold;
    /** highlighted colors */
    background-color: #0e4b46;
    border-color: #28e2d2;
}

/** Entry icon */
element-icon {
    /** change size to 128 pixels. */
    size: 128px;
    cursor: inherit;
}


