# About SKRAMM

**Skramm** is a set of configuration files designed to create a full blown Linux Media Center.  
As of now, Skramm is designed to be used with a mini keyboard, or keyboard and mouse, although using a remote-control might be possible (but not tested yet.)  
This is a personal project that might be of interest to others. So test at your own risk, and consider it an alpha version.

The Skramm project aims towards the KISS philosophy. It is aimed at using light, open source, and easily available tools in order to build a coherent default graphical framework.  
Skramm will recommend default tools, but every user should be able to customize it and set their favorite tools inside it.

Skramm : because "just enough OS for Kodi" is not enough.

# Installing Skramm

It should be safe to deploy it over an existing Linux installation, as long the required dependencies can be installed on your distribution. 
**However, keep in mind it will replace existing configuration files if they exist.** If you already have your own config for i3/rofi and so on... You might want to try this on a fresh install instead.



# Dependencies

i3

picom

Rofi (dotfiles will add rofi-json and rofi-power-menu)

jq (for rofi-json)

Nitrogen

lxpolkit ( https://www.reddit.com/r/i3wm/comments/chjnng/how_do_i_fix_authentication_agent_in_i3wm/ )

# Recommended software

dmenu

conky

lxappearance

Goodvibes

Freetube (not available from all repos. Get it here : https://freetubeapp.io/#download )

Kodi

Retropie (can be installed on debian/ubuntu)

## browser plugins

**vimium**

**HNTP** : https://github.com/ibillingsley/HumbleNewTabPage
